package com.cryptopm.config;

import com.binance.connector.client.WebsocketApiClient;
import com.binance.connector.client.impl.WebsocketApiClientImpl;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CryptoPMConfig {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public WebsocketApiClient websocketApiClient() {
        return new WebsocketApiClientImpl();
    }
}
