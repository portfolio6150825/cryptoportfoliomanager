package com.cryptopm.service;

import com.binance.connector.client.WebsocketApiClient;
import com.cryptopm.entity.TickerEntity;
import com.cryptopm.repository.TickerRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
public class BinanceWebsocketService {

    private final WebsocketApiClient client;
    private final TickerRepository tickerRepository;
    private final ObjectMapper objectMapper;

    private static final String CRYPTO_PAIRS_FILE = "/static/crypto_pairs.txt";
    private static final String PAIRS_NODE = "result";

    @Autowired
    public BinanceWebsocketService(WebsocketApiClient client, TickerRepository tickerRepository, ObjectMapper objectMapper) {
        this.client = client;
        this.tickerRepository = tickerRepository;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void fetchMarketData() throws IOException {
        List<String> pairs = Files.readAllLines(new ClassPathResource(CRYPTO_PAIRS_FILE).getFile().toPath());
        client.connect(event -> {
            try {
                JsonNode jsonNode = objectMapper.readTree(event);
                List<TickerEntity> tickers = objectMapper.convertValue(jsonNode.get(PAIRS_NODE), new TypeReference<>() {});
                List<TickerEntity> updatedTickers = tickers.stream().map(this::updateTicker).toList();
                tickerRepository.saveAll(updatedTickers);
            } catch (JsonProcessingException e) {
                throw new RuntimeException("Error while parsing ticker data!");
            }
        });

        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() ->
                client.market().ticker(new JSONObject(Map.of("symbols", pairs))), 0, 10, TimeUnit.SECONDS);
    }

    private TickerEntity updateTicker(TickerEntity ticker) {
        TickerEntity tickerEntity = tickerRepository.findBySymbol(ticker.getSymbol());
        if (tickerEntity == null) tickerEntity = ticker;
        tickerEntity.setSymbol(ticker.getSymbol());
        tickerEntity.setPriceChange(ticker.getPriceChange());
        tickerEntity.setPriceChangePercent(ticker.getPriceChangePercent());
        tickerEntity.setWeightedAvgPrice(ticker.getWeightedAvgPrice());
        tickerEntity.setOpenPrice(ticker.getOpenPrice());
        tickerEntity.setHighPrice(ticker.getHighPrice());
        tickerEntity.setLowPrice(ticker.getLowPrice());
        tickerEntity.setVolume(ticker.getVolume());
        tickerEntity.setCount(ticker.getCount());
        tickerEntity.setLastPrice(ticker.getLastPrice());
        tickerEntity.setQuoteVolume(ticker.getQuoteVolume());
        tickerEntity.setLastId(ticker.getLastId());
        tickerEntity.setOpenTime(ticker.getOpenTime());
        tickerEntity.setFirstId(ticker.getFirstId());
        tickerEntity.setCloseTime(ticker.getCloseTime());
        return tickerEntity;
    }

    @PreDestroy
    public void close() {
        client.close();
    }
}