package com.cryptopm.service;

import com.cryptopm.dto.TickerDTO;
import com.cryptopm.repository.TickerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TickerService {

    private final ModelMapper modelMapper;

    private final TickerRepository tickerRepository;

    @Autowired
    public TickerService(ModelMapper modelMapper, TickerRepository tickerRepository) {
        this.modelMapper = modelMapper;
        this.tickerRepository = tickerRepository;
    }

    public List<TickerDTO> getAllTickers() {
        return tickerRepository.findAll()
                .stream()
                .map(tickerEntity -> modelMapper.map(tickerEntity, TickerDTO.class))
                .toList();
    }

    public TickerDTO getTickerBySymbol(String symbol){
        return modelMapper.map(tickerRepository.findBySymbol(symbol), TickerDTO.class);

    }


}
