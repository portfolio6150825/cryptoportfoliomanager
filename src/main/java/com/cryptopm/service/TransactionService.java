package com.cryptopm.service;

import com.cryptopm.dto.TransactionDTO;
import com.cryptopm.entity.TransactionEntity;
import com.cryptopm.repository.TransactionRepository;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final ModelMapper modelMapper;


    @Autowired
    public TransactionService(TransactionRepository transactionRepository, ModelMapper modelMapper) {
        this.transactionRepository = transactionRepository;
        this.modelMapper = modelMapper;
    }

    public List<TransactionDTO> getTransactions() {
        return transactionRepository.findAll()
                .stream()
                .map(transactionEntity -> modelMapper.map(transactionEntity, TransactionDTO.class))
                .toList();
    }

    public TransactionDTO getTransaction(UUID uuid) {
        return modelMapper.map(transactionRepository.findByUuid(uuid), TransactionDTO.class);
    }

    public void deleteTransactionByUuid(UUID uuid) {
        transactionRepository.deleteByUuid(uuid);
    }

    public TransactionDTO addTransaction(TransactionDTO transactionDTO) {
        TransactionEntity transactionEntity = transactionRepository.save(modelMapper.map(transactionDTO, TransactionEntity.class));
        return modelMapper.map(transactionEntity, TransactionDTO.class);
    }

    public Map<String, List<TransactionDTO>> aggregateTransactions() {
        return transactionRepository.findAll()
                .stream()
                .map(transactionEntity -> modelMapper.map(transactionEntity, TransactionDTO.class))
                .collect(Collectors.groupingBy(TransactionDTO::getSymbol));
    }
}
