package com.cryptopm.service;

import com.cryptopm.dto.AssetDTO;
import com.cryptopm.dto.PortfolioDTO;
import com.cryptopm.dto.TickerDTO;
import com.cryptopm.dto.TransactionDTO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
public class PortfolioService {

    private final  TransactionService transactionService;
    private final  TickerService tickerService;

    public PortfolioService(TransactionService transactionService, TickerService tickerService) {
        this.transactionService = transactionService;
        this.tickerService = tickerService;
    }


    public PortfolioDTO getPortfolio() {
        PortfolioDTO portfolioDTO = new PortfolioDTO();
        transactionService.aggregateTransactions().forEach((symbol, transactions) -> {
            AssetDTO assetDTO = new AssetDTO();
            assetDTO.setSymbol(symbol);

            BigDecimal holding = calculateHolding(transactions);
            BigDecimal averageBuyPrice = calculateAverageBuyPrice(transactions);

            TickerDTO tickerBySymbol = tickerService.getTickerBySymbol(symbol);
            assetDTO.setPrice(tickerBySymbol.getLastPrice());
            assetDTO.setAverageBuyPrice(averageBuyPrice);
            assetDTO.setHolding(holding);

            BigDecimal profitOrLoss = calculateProfitOrLoss(tickerBySymbol.getLastPrice(), holding, averageBuyPrice);
            assetDTO.setProfitOrLoss(profitOrLoss);
            assetDTO.setPercent(calculatePercent(profitOrLoss, holding, averageBuyPrice));

            portfolioDTO.addAsset(assetDTO);
        });
        return portfolioDTO;
    }

    private BigDecimal calculateHolding(List<TransactionDTO> transactionList) {
        return transactionList.stream()
                .map(TransactionDTO::getQuantity)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateAverageBuyPrice(List<TransactionDTO> transactionList) {
        return transactionList.stream()
                .map(TransactionDTO::getTotalAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(calculateHolding(transactionList), 2, RoundingMode.HALF_UP);
    }

    private BigDecimal calculateProfitOrLoss(BigDecimal lastPrice, BigDecimal holding, BigDecimal averageBuyPrice) {
        return lastPrice.multiply(holding).subtract(averageBuyPrice.multiply(holding)).setScale(2, RoundingMode.HALF_UP);
    }

    private String calculatePercent(BigDecimal profitOrLoss, BigDecimal holding, BigDecimal averageBuyPrice) {
        BigDecimal percent = profitOrLoss.divide(holding.multiply(averageBuyPrice), 12, RoundingMode.HALF_UP).multiply(new BigDecimal(100));
        return String.format("%.2f %%", percent);
    }
}
