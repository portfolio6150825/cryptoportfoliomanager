package com.cryptopm.entity;

import com.cryptopm.model.TransactionType;
import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@Table(name = "transactions")

public class TransactionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid")
    private UUID uuid;

    @Column(name = "symbol")
    private String symbol;

    @Column(name = "transaction_type")
    private TransactionType transactionType;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @Column(name = "price_per_coin")
    private BigDecimal pricePerCoin;

    @Column(name = "purchase_date")
    private Date purchaseDate;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @PrePersist
    public void onCreate() {
        uuid = UUID.randomUUID();
        totalAmount = quantity.multiply(pricePerCoin);
    }
}
