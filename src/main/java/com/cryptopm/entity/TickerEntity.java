package com.cryptopm.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.math.BigInteger;

@Entity
@Data
@Table(name = "tickers")
public class TickerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "symbol")
    private String symbol;
    @Column(name = "price_change")
    private BigDecimal priceChange;
    @Column(name = "price_change_percent")
    private BigDecimal priceChangePercent;
    @Column(name = "weighted_avg_price")
    private BigDecimal weightedAvgPrice;
    @Column(name = "open_price")
    private BigDecimal openPrice;
    @Column(name = "high_price")
    private BigDecimal highPrice;
    @Column(name = "low_price")
    private BigDecimal lowPrice;
    @Column(name = "last_price")
    private BigDecimal lastPrice;
    @Column(name = "volume")
    private BigDecimal volume;
    @Column(name = "quote_volume")
    private BigDecimal quoteVolume;
    @Column(name = "open_time")
    private BigInteger openTime;
    @Column(name = "close_time")
    private BigInteger closeTime;
    @Column(name = "first_id")
    private BigInteger firstId;
    @Column(name = "last_id")
    private BigInteger lastId;
    @Column(name = "count")
    private BigInteger count;

}
