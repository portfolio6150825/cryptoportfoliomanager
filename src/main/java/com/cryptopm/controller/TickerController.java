package com.cryptopm.controller;

import com.cryptopm.dto.TickerDTO;
import com.cryptopm.service.TickerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tickers")
public class TickerController {

    private final TickerService tickerService;

    public TickerController(TickerService tickerService) {
        this.tickerService = tickerService;
    }

    @GetMapping
    public List<TickerDTO> getAllTickers() {
        return tickerService.getAllTickers();
    }

    @GetMapping("/{symbol}")
    public TickerDTO getTicker(@PathVariable("symbol") String symbol) {
        return tickerService.getTickerBySymbol(symbol);
    }
}
