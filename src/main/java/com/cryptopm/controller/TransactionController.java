package com.cryptopm.controller;

import com.cryptopm.dto.TransactionDTO;
import com.cryptopm.service.TransactionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


    @GetMapping
    public List<TransactionDTO> getAllTransactions() {

        return transactionService.getTransactions();
    }

    @GetMapping("/{uuid}")
    public TransactionDTO getTransactionByUuid(@PathVariable("uuid") UUID uuid) {

        return transactionService.getTransaction(uuid);
    }

    @DeleteMapping("/{uuid}")
    public void deleteByUuid(@PathVariable("uuid") UUID uuid) {
        transactionService.deleteTransactionByUuid(uuid);
    }

    @PostMapping
    public TransactionDTO addTransaction(@RequestBody TransactionDTO transactionDTO) {
        return transactionService.addTransaction(transactionDTO);
    }


}
