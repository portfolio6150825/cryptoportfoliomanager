package com.cryptopm.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class AssetDTO {

    private String symbol;
    private BigDecimal price;
    private BigDecimal holding;
    private BigDecimal averageBuyPrice;
    private BigDecimal profitOrLoss;
    private String percent;
}
