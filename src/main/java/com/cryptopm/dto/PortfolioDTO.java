package com.cryptopm.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class PortfolioDTO {

    private List<AssetDTO> assets = new ArrayList<>();

    public void addAsset(AssetDTO assetDTO) {
        assets.add(assetDTO);
    }
}
