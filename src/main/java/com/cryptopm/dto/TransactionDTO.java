package com.cryptopm.dto;

import com.cryptopm.model.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
public class TransactionDTO {
    private UUID uuid;
    private String symbol;
    private TransactionType transactionType;
    private BigDecimal quantity;
    private BigDecimal pricePerCoin;
    private Date purchaseDate;
    private BigDecimal totalAmount;

}
