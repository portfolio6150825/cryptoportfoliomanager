package com.cryptopm.model;

public enum TransactionType {

    BUY,
    SELL
}
