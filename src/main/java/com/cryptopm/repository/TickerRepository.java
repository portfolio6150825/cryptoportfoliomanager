package com.cryptopm.repository;

import com.cryptopm.entity.TickerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TickerRepository extends JpaRepository<TickerEntity, Long> {

    TickerEntity findBySymbol(String symbol);

}
