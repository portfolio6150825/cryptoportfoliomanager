package com.cryptopm.repository;

import com.cryptopm.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity,Long> {

    TransactionEntity findByUuid(UUID uuid);

    void deleteByUuid(UUID uuid);



}
