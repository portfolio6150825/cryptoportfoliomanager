package com.cryptopm.service;

import com.cryptopm.dto.AssetDTO;
import com.cryptopm.dto.PortfolioDTO;
import com.cryptopm.dto.TickerDTO;
import com.cryptopm.dto.TransactionDTO;
import com.cryptopm.model.TransactionType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class PortfolioServiceTest {

    @Mock
    private TransactionService transactionService;
    @Mock
    private TickerService tickerService;

    private PortfolioService portfolioService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        portfolioService = new PortfolioService(transactionService, tickerService);
    }

    @Test
    void getPortfolio_shouldGeneratePortfolioSummary() {
        List<TransactionDTO> transactionList1 = Arrays.asList(
                TransactionDTO.builder()
                        .uuid(UUID.randomUUID())
                        .symbol("BTCUSDT")
                        .transactionType(TransactionType.BUY)
                        .quantity(new BigDecimal("5.00"))
                        .pricePerCoin(new BigDecimal("21640.00"))
                        .purchaseDate(Date.from(Instant.now()))
                        .totalAmount(new BigDecimal("108200.00"))
                        .build(),
                TransactionDTO.builder()
                        .uuid(UUID.randomUUID())
                        .symbol("BTCUSDT")
                        .transactionType(TransactionType.BUY)
                        .quantity(new BigDecimal("500.00"))
                        .pricePerCoin(new BigDecimal("31000.00"))
                        .purchaseDate(Date.from(Instant.now()))
                        .totalAmount(new BigDecimal("15500000.00"))
                        .build()
        );
        Map<String, List<TransactionDTO>> transactions = new HashMap<>();
        transactions.put("BTCUSDT", transactionList1);


        TickerDTO tickerBTC = TickerDTO.builder()
                .symbol("BTCUSDT")
                .lastPrice(new BigDecimal("40000.00"))
                .build();


        when(transactionService.aggregateTransactions()).thenReturn(transactions);
        when(tickerService.getTickerBySymbol("BTCUSDT")).thenReturn(tickerBTC);

        PortfolioDTO portfolioDTO = portfolioService.getPortfolio();

        assertEquals(1, portfolioDTO.getAssets().size());

        AssetDTO btcAsset = portfolioDTO.getAssets().get(0);
        assertEquals("BTCUSDT", btcAsset.getSymbol());
        assertEquals(new BigDecimal("40000.00"), btcAsset.getPrice());
        assertEquals(new BigDecimal("505.00"), btcAsset.getHolding());
        assertEquals(new BigDecimal("30907.33"), btcAsset.getAverageBuyPrice());
        assertEquals(new BigDecimal("4591798.35"), btcAsset.getProfitOrLoss());
        assertEquals("29.42 %", btcAsset.getPercent());
    }

}